// //SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.3;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";

/// @custom:security-contact it@perks.id
contract LoyalDappToken is ERC20, ERC20Burnable, AccessControl {
    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");

    uint256 private immutable _cap;

    event LoyalDappTokensMinted(address to, uint256 amount);

    constructor(uint256 cap_, address multisig_minter, address multisig_admin) ERC20("LoyalDapp", "LDP") {
        require(cap_ > 0, "Cap cannot be less than zero");

        _grantRole(DEFAULT_ADMIN_ROLE, multisig_admin); // Add Multi Signature Admin Address
        _grantRole(MINTER_ROLE, multisig_minter); // Add Multi Signature Minter Address
        _cap = cap_;
    }

    function cap() public view virtual returns (uint256) {
        return _cap;
    }

    function mint(address to, uint256 amount) public onlyRole(MINTER_ROLE) {
        require(_cap >= totalSupply() + amount, "LoyalDapp Token supply limit reached");
        _mint(to, amount);
        emit LoyalDappTokensMinted(to, amount);
    }

}
