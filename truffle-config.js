const HDWalletProvider = require("@truffle/hdwallet-provider");
const fs = require("fs");
const mnemonic = fs.readFileSync(".secret").toString().trim();
const bscApiKey = fs.readFileSync(".bscApiSecret").toString().trim();
const polygonApiKey = fs.readFileSync(".polygonApiSecret").toString().trim();

module.exports = {
    // See <http://truffleframework.com/docs/advanced/configuration>
    // to customize your Truffle configuration!
    networks: {
        development: {
            host: "127.0.0.1", // Localhost (default: none)
            port: 7545, // Standard Ethereum port (default: none)
            network_id: "*" // Any network (default: none)
        },
        testnet_bsc: {
            provider: () => new HDWalletProvider(mnemonic, `https://data-seed-prebsc-1-s1.binance.org:8545`),
            network_id: 97,
            confirmations: 10,
            timeoutBlocks: 2000,
            skipDryRun: true,
            networkCheckTimeout: 999999,
        },
        testnet_polygon: {
            provider: () => new HDWalletProvider(mnemonic, `https://rpc-mumbai.maticvigil.com`),
            network_id: 80001,
            confirmations: 10,
            timeoutBlocks: 2000,
            skipDryRun: true,
            networkCheckTimeout: 999999,
        },
        bsc: {
            provider: () => new HDWalletProvider(mnemonic, `https://bsc-dataseed1.binance.org`),
            network_id: 56,
            confirmations: 10,
            timeoutBlocks: 200,
            skipDryRun: true
        },
        polygon: {
            provider: () => new HDWalletProvider(mnemonic, `https://polygon-rpc.com`),
            network_id: 137,
            confirmations: 10,
            timeoutBlocks: 200,
            skipDryRun: true
        },
    },
    compilers: {
        solc: {
            version: "0.8.11"
        }
    },

    plugins: [
        'truffle-plugin-verify'
    ],
    api_keys: {
        bscscan: bscApiKey,
        polygonscan: polygonApiKey
    }
};